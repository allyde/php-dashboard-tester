FROM php:7.0-cli

RUN apt-get update && apt-get install -y unzip libfreetype6-dev libjpeg62-turbo-dev libpng12-dev libssl-dev libmcrypt-dev libreadline-dev
RUN pecl install mongodb
RUN docker-php-ext-install -j$(nproc) gd mbstring mcrypt mysqli
RUN docker-php-ext-enable mongodb gd mbstring mcrypt mysqli
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
